# Mitt program

Hei, og velkommen til spillet Snake!
Det er mulig du har spilt det før og skjønner konseptet, men jeg forteller uansett hvordan det fungerer. Når du kjører spillet vil du først få opp en meny der du får beskjed om å trykke "Space" for å starte det. En ting du må være obs på når du trykker "Space" er at slangen vil automatisk begynne å gå mot venstre på brettet, så hold fingrene dine klare på piltastene, for det er disse du stryrer slangen rundt på brettet med!

Reglene går som følger: 
1: Du stryrer hodet til slangen med piltastene, kroppen vil følge etter.
2: Spis så mange epler du klarer for å gro slangen å forbedre poengsummen din.
3: Ikke beveg slangen ut av brettet eller inn i seg selv, da taper du!
4: For hvert tiende eple du spiser vil slangens fart øke og det vil bli noe vanskeligere.
5: Etter eple er spist vil eple gå gjennom hele slangen før halen til slangen gror en rute.
6: Hvis du taper og får "GAME OVER" kan du trykke på "R" for å komme tilbake til hovedmenyen. 

Det var en kjapp og enkel oppsumering av hvordan Snake fungerer. Lykke til og god fornøyelse!

Her er en lenke til en videodemonstrasjon av spillet:
https://youtu.be/2s3-CdGtUZQ
