package no.uib.inf101.sem2.snake.model.snakegame;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

import no.uib.inf101.sem2.grid.CellPosition;
import no.uib.inf101.sem2.grid.GridCell;

public class Snake implements Iterable<GridCell<Character>>{
    private char color;
    private CellPosition pos;
    private ArrayList<CellPosition> snakeBody;

    //Konstruktør

    private Snake(char color, ArrayList<CellPosition> snakeBody, CellPosition pos){
        this.color = color;
        this.snakeBody = snakeBody;
        this.pos = pos;
        
    }

    /**
     * Metode som får slangen på brettet. Inneholder utgangsposisjonen til slangen.
     * Metoden definerer også slangen med char S
     * @param color char til slangen ('S').
     * @return
     * @throws IllegalArgumentException Hvis char ikke er S.
     */
    public static Snake snakeOnBoard(char color) throws IllegalArgumentException{
        CellPosition pos = new CellPosition(7, 7);
        ArrayList<CellPosition> snakeBody;

        switch(color) {
            case 'S' -> {
                snakeBody = new ArrayList<CellPosition>();
                snakeBody.add(new CellPosition(7, 7));
                snakeBody.add(new CellPosition(7, 8));
                snakeBody.add(new CellPosition(7, 9));
            }
            
            default -> throw new IllegalArgumentException("No available tetromino for '" + color + "'");
        }
        return new Snake(color, snakeBody, pos);
    }
    
    /**
     * Metode som henter en posisjon. Bruker denne til å bevege slangen i modellen.
     * @return CellPosition
     */ 
    public CellPosition getPos(){
        return pos;
    }

    /**
     * Metode som sier hva som skjer når slangen beveger seg.
     * Den legger til og fjerner en posisjon i slangen slik at den verken vokser eller minker.
     * @param pos CellPosition
     */
    public void move(CellPosition pos){
        this.pos = pos;
        snakeBody.add(0, pos);
        snakeBody.remove(snakeBody.size() - 1);
    }

    /**
     * Metode som gjør de samme som den over, bare at den
     * aldri fjerner noe, men at den gror i stedet.
     * @param pos CellPosition som legges til i slangen
     */
    public void grow(CellPosition pos){
        this.pos = pos;
        snakeBody.add(0, pos);
    }

    /**
     * Metode som sjekker om slangen krasjer i seg selv.
     * @param pos CellPosition som allerede er i slangen eller ikke
     * @return
     */
    public boolean isSnakeHere(CellPosition pos) {
        if (snakeBody.contains(pos)) {
            return true;
        }
        else {
            return false;
        }

    }
    
    //IMPLEMENTERTE OVERRIDE-METODER

    @Override
    public Iterator<GridCell<Character>> iterator() {

        List<GridCell<Character>> snakeList = new ArrayList<>();

        for(int i = 0 ; i < snakeBody.size() ; i++) {
            snakeList.add(new GridCell<Character>(snakeBody.get(i),color));
        }
        return snakeList.iterator();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof Snake)) {
            return false;
        }
        Snake other = ((Snake) obj);
        return (color == other.color) && (snakeBody.equals(other.snakeBody)) && (pos.equals(other.pos));
    }

    @Override
    public int hashCode() {
        return Objects.hash(color, snakeBody, pos);
    }
}
