package no.uib.inf101.sem2.snake.view;

import no.uib.inf101.sem2.grid.GridCell;
import no.uib.inf101.sem2.grid.GridDimension;
import no.uib.inf101.sem2.snake.model.GameState;

public interface ViewableSnakeModel {
    /**
     * 
     * @return GridDimension objekt
     */
    GridDimension getDimension();


    /**
     * 
     * @return Objekt, som når iterert gir posisjon på brettet
     */
    Iterable<GridCell<Character>> getTilesOnBoard();

    /**
     * 
     * @return Objekt, som gir posisjonen til slangen på brettet
     */
    Iterable<GridCell<Character>> getSnakeBody();

    /**
     * Metode som sier hvor mange epler som er spist
     * @return int
     */
    int getApplesEaten();

    /**
     * 
     * @return GameState objekt, enten GAME_MENU, ACTIVE_GAME eller GAME_OVER
     */
    GameState getGameState();
}
