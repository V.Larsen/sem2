package no.uib.inf101.sem2.snake.view;

import java.awt.Color;

public interface ColorTheme {
    /**
     * Kaster exception når return-verdi er null eller udefinert
     * @param c
     * @return color objekt
     * @throws IllegalArgumentException
     */
    Color getCellColor(char c) throws IllegalArgumentException;

    /**
     * Metode som bestemmer fargen rundt selve brettet
     * @return Color objekt, fargen som skal vises
     */
    Color getFrameColor();

    /**
     * Metode som bestemmer fargen i bakgrunnen av brettet
     * @return Color objekt, fargen som skal vises
     */
    Color getBackgroundColor();

    /**
     * Metode som bestemmer fargen over brettet når spillet er ferdig
     * @return Color objekt, fargen som skal vises
     */
    Color getGameOverColor();

    /**
     * Metode som bestemmer fargen på "GAME OVER"-skriften når spillet er ferdig
     * @return Color objekt, fargen som skal vises
     */
    Color gameOverTextColor();

    /**
     * Metode som bestemmer fargen på scoren over brettet.
     * @return Color objekt, fargen som skal vises
     */
    Color scoreBoardColor();

}
