package no.uib.inf101.sem2.snake.model;

import no.uib.inf101.sem2.grid.CellPosition;
import no.uib.inf101.sem2.grid.GridCell;
import no.uib.inf101.sem2.grid.GridDimension;
import no.uib.inf101.sem2.snake.controller.ControllableSnakeModel;
import no.uib.inf101.sem2.snake.model.snakegame.Food;
import no.uib.inf101.sem2.snake.model.snakegame.Snake;
import no.uib.inf101.sem2.snake.view.ViewableSnakeModel;

public class SnakeModel implements ViewableSnakeModel, ControllableSnakeModel{
    private SnakeBoard snakeBoard;
    private Snake snake;
    private int applesEaten;
    private int time;
    private GameState gameState;

    //Konstruktør

    public SnakeModel(SnakeBoard snakeBoard) {
        this.gameState = GameState.GAME_MENU;
        this.snakeBoard = snakeBoard;
        this.snake = Snake.snakeOnBoard('S');
        this.time = 200;
        placeNewApple();
    }

    /**
     * Metode som setter GameState, brukes i kontrollen
     * @param gameState Hvilken gamestate som blir satt
     */
    public void setGameState(GameState gameState) {
            this.gameState = gameState;
        }

    //IMPLEMENTERTE OVERRIDE-METODER

    @Override
        public boolean moveSnake(int deltaRow, int deltaCol) {
            CellPosition newPos = new CellPosition(snake.getPos().row() + deltaRow, snake.getPos().col() + deltaCol);
            boolean moved;

            if(isLegalMove(newPos)) {
                snake.move(newPos);
                moved = true;

                if(touchedApple(newPos)){
                    snake.grow(newPos);
                    placeNewApple();
                    eatApple(newPos);
                    this.applesEaten++;
                    moved = true;
                    //Oppdaterer this.time slik at slangen vil gå litt fortere for hvert 10ende eple den spiser.
                    if (this.applesEaten % 10 == 0) this.time = this.time * 9/10;
                }
                
            }
            else {
                moved = false;
                this.gameState = GameState.GAME_OVER;
            }
            return moved;
        }

    @Override
    public GridDimension getDimension() {
        return this.snakeBoard;
    }

    @Override
    public Iterable<GridCell<Character>> getTilesOnBoard() {
        return this.snakeBoard;
    }

    @Override
    public Iterable<GridCell<Character>> getSnakeBody() {
        return this.snake;
    }

    
    @Override
    public int getApplesEaten(){
        return this.applesEaten;
    }


    @Override
    public int clockBeat() {
        return this.time;
    }

    @Override
    public GameState getGameState() {
        return this.gameState;
    }

    

    //PRIVATE HJELPE-METODER

    private boolean isLegalMove(CellPosition pos){
        if (!snakeBoard.positionIsOnGrid(pos) || snake.isSnakeHere(pos)) 
            return false;
        else {
            return true;
        }
        
    }

    private boolean touchedApple(CellPosition pos){
        return snakeBoard.get(pos)=='A';
    }
    
    private void eatApple(CellPosition pos) {
        snakeBoard.set(pos, '-');
    }

    private void placeNewApple(){
        Food food = new Food(snakeBoard.rows(), snakeBoard.cols());
        CellPosition pos = food.newFoodLocation();
        while(!isLegalMove(pos)){
            pos = food.newFoodLocation();
        }
        snakeBoard.set(pos, 'A');
    }
    
}

    

