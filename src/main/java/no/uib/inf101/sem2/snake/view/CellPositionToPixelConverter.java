package no.uib.inf101.sem2.snake.view;

import java.awt.geom.Rectangle2D;

import no.uib.inf101.sem2.grid.CellPosition;
import no.uib.inf101.sem2.grid.GridDimension;

public class CellPositionToPixelConverter {
    private Rectangle2D box;
    private GridDimension gd;
    private double margin;

  //Konstruktør
    public CellPositionToPixelConverter(Rectangle2D box, GridDimension gd, double margin) {
    this.box = box;
    this.gd = gd;
    this.margin = margin;
  }

  /**
   * Metode som henter koordinatene til cellene i brettet
   * @param cp Posisjonen til cellen (rad og kolonne)
   * @return Rectangle2D-objekt som definerer hvor cellen er i brettet (x,y) og hvilken dimensjon (width x height).
   */
  public Rectangle2D getBoundsForCell(CellPosition cp) {
    double cellWidth = (box.getWidth() - (gd.cols() + 1) * margin) / gd.cols();
    double cellHeight = (box.getHeight() - (gd.rows() + 1) * margin) / gd.rows();
    double cellX = box.getX() + margin + cp.col() * (margin + cellWidth);
    double cellY = box.getY() + margin + cp.row() * (margin + cellHeight);
    return new Rectangle2D.Double(cellX, cellY, cellWidth, cellHeight);
  }
}
