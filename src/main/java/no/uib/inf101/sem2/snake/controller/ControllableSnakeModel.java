package no.uib.inf101.sem2.snake.controller;

import no.uib.inf101.sem2.snake.model.GameState;

public interface ControllableSnakeModel {

    /**
     * Metode som flytter hodet til slangen gitt antall rader og kolonner 
     * og oppdaterer slangen
     * @param deltaRow Antall rader man vil flytte
     * @param deltaCol Antall kolonner man vil flytte
     * @return True hvis flyttet faktisk skjedde, false ellers
     */
    boolean moveSnake(int deltaRow, int deltaCol);

    /**
     * 
     * @return int som sier hvor mange ms det tar før slangen flytter seg
     */
    int clockBeat();

    /**
     * 
     * @return GameState objekt som enten er ACTIVE_GAME eller GAME_OVER
     */
    GameState getGameState();
}
