package no.uib.inf101.sem2.snake;

import javax.swing.JFrame;

import no.uib.inf101.sem2.snake.controller.SnakeController;

import no.uib.inf101.sem2.snake.model.SnakeBoard;
import no.uib.inf101.sem2.snake.model.SnakeModel;
import no.uib.inf101.sem2.snake.view.SnakeView;

public class Main {
  public static void main(String[] args) {
    SnakeBoard grid = new SnakeBoard(15, 15);
    SnakeModel snakeModel = new SnakeModel(grid);
    SnakeView view = new SnakeView(snakeModel);
    
    new SnakeController(snakeModel, view);

    JFrame frame = new JFrame();
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setTitle("INF101");
    frame.setContentPane(view);
    frame.pack();
    frame.setVisible(true);
  }
}
