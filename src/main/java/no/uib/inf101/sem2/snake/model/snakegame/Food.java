package no.uib.inf101.sem2.snake.model.snakegame;

import java.util.Random;

import no.uib.inf101.sem2.grid.CellPosition;

public class Food{
    private int appleX;
    private int appleY;
    private int rows;
    private int cols;

    //Konstruktør

    public Food(int rows, int cols) {
        this.rows = rows;
        this.cols = cols;
    }

    /**
     * Metode som lager genererer to tilfeldig tall
     * basert på antall rader og kolonner. 
     * Denne metoden brukes for å finne neste lokasjon for eple
     * @return CellPosition som tar inn de to genererte tallene
     */
    public CellPosition newFoodLocation() {
        Random random = new Random();
        appleX = random.nextInt(cols);
        appleY = random.nextInt(rows);
        return new CellPosition(appleX, appleY);
    }

}
