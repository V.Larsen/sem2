package no.uib.inf101.sem2.snake.view;

import java.awt.Color;

public class DefaultColorTheme implements ColorTheme{

    //IMPLEMENTERTE OVERRIDE-METODER

    @Override
    public Color getCellColor(char c) {
        Color color = switch(c) {
            case 'S' -> Color.decode("#038914");
            case 'A' -> Color.RED;
            case '-' -> Color.decode("#6EF07E");
            default -> throw new IllegalArgumentException("No available color for '" + c + "'");
        };
        return color;
    }

    @Override
    public Color getFrameColor() {
        return new Color(0,0,0,0);
    }

    @Override
    public Color getBackgroundColor() {
        return null;
    }

    @Override
    public Color getGameOverColor() {
        return new Color(0, 0, 0, 128);
    }

    @Override
    public Color gameOverTextColor() {
        return Color.WHITE;
    }

    @Override
    public Color scoreBoardColor() {
        return Color.BLACK;
    }
}
