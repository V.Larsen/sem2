package no.uib.inf101.sem2.snake.model;

import no.uib.inf101.sem2.grid.CellPosition;
import no.uib.inf101.sem2.grid.Grid;
import no.uib.inf101.sem2.grid.GridCell;

public class SnakeBoard extends Grid<Character>{
    
    //Konstruktør
    public SnakeBoard(int rows, int cols) {
        super(rows, cols);

        for(GridCell<Character> i : this) {
            this.set(i.pos(), '-');
        }
    }

    /**
     * Metode som går gjennom hver posisjon i brettet og plusser verdien i den posisjonen i en string.
     * Brukes egentlig kun for testing av brettet.
     * @return String bestående av verdiene på brettet
     */
    public String prettyString() {
        String resultString = "";
        for (int i = 0 ; i < rows() ; i++) {
            String row = "";
            for(int j = 0 ; j < cols() ; j++) {
                row += this.get(new CellPosition(i, j));
            }
            if (i == rows() - 1) {
                resultString += row;
            } 
            else {
                resultString += row + "\n";
            }
        }
        return resultString;
    }
}
