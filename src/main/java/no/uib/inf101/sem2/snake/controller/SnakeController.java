package no.uib.inf101.sem2.snake.controller;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JFrame;
import javax.swing.Timer;

import no.uib.inf101.sem2.snake.model.GameState;
import no.uib.inf101.sem2.snake.model.SnakeBoard;
import no.uib.inf101.sem2.snake.model.SnakeModel;
import no.uib.inf101.sem2.snake.view.SnakeView;


public class SnakeController implements KeyListener{
    private SnakeModel snakeModel;
    //private ControllableSnakeModel snakeModel;
    private SnakeView snakeView;
    private Timer timer;
    private char direction;

    //Konstruktør

    public SnakeController(SnakeModel snakeModel, SnakeView snakeView) {
        this.snakeView = snakeView;
        this.snakeModel = snakeModel;
        snakeView.setFocusable(true);
        snakeView.addKeyListener(this);
        this.timer = new Timer(snakeModel.clockBeat(), this::clockTick);
        this.timer.start();
    }

    //IMPLEMENTERTE OVERRIDE-METODER

    @Override
    public void keyTyped(KeyEvent e) {
        
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (snakeModel.getGameState() == GameState.GAME_OVER) {
            if (e.getKeyCode() == KeyEvent.VK_R) {
                snakeModel.setGameState(GameState.GAME_MENU);
                SnakeBoard grid = new SnakeBoard(15, 15);
                SnakeModel snakeModel = new SnakeModel(grid);
                SnakeView view = new SnakeView(snakeModel);
    
                new SnakeController(snakeModel, view);

                JFrame frame = new JFrame();
                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                frame.setTitle("INF101");
                frame.setContentPane(view);
                frame.pack();
                frame.setVisible(true);
            }
        }
        if (snakeModel.getGameState() == GameState.GAME_MENU){
            if (e.getKeyCode() == KeyEvent.VK_SPACE) {
                // SPACE was pressed

                snakeModel.setGameState(GameState.ACTIVE_GAME);
                direction = 'L'; 
            }
        }

        if (snakeModel.getGameState() == GameState.ACTIVE_GAME)
            if (e.getKeyCode() == KeyEvent.VK_LEFT) {
                // Venstre-pil
                if (!(direction == 'R')) direction = 'L'; 
            }
            else if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
                // Høyre-pil
                if (!(direction == 'L')) direction = 'R';
            }
            else if (e.getKeyCode() == KeyEvent.VK_DOWN) {
                // Nedover-pil
                if (!(direction == 'U')) direction = 'D';
            }
            else if (e.getKeyCode() == KeyEvent.VK_UP) {
                // Oppover-pil
                if (!(direction == 'D')) direction = 'U';
            }

            snakeView.repaint();
        
    }

    @Override
    public void keyReleased(KeyEvent e) {
        
    }

    //PRIVATE HJELPEMETODER

    private void clockTick(ActionEvent actionEvent) {
        if (snakeModel.getGameState() == GameState.ACTIVE_GAME)
            if (direction == 'L') {
                // Left arrow was pressed
                    snakeModel.moveSnake(0, -1); 
                }
            else if (direction == 'R') {
                // Right arrow was pressed
                snakeModel.moveSnake(0, 1);
            }
            else if (direction == 'D') {
                // Down arrow was pressed
                snakeModel.moveSnake(1, 0);
            }
            else if (direction == 'U') {
                // Up arrow was pressed
                snakeModel.moveSnake(-1, 0);
            }
            this.timer.setDelay(snakeModel.clockBeat());
            
            snakeView.repaint();

    }
}
    

