package no.uib.inf101.sem2.snake.model;

/**
 * Enum som inneholder tilstanden til spillet. 
 */
public enum GameState {
    GAME_MENU,
    ACTIVE_GAME,
    GAME_OVER
}
