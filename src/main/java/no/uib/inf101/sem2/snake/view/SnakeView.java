package no.uib.inf101.sem2.snake.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;

import javax.swing.JPanel;

import no.uib.inf101.sem2.grid.GridCell;
import no.uib.inf101.sem2.snake.model.GameState;

public class SnakeView extends JPanel {
    private ViewableSnakeModel viewableSnakeModel;
    private static final double OUTERMARGIN = 40;
    private static final double cellMargin = 1;
    private ColorTheme colorTheme;

    //Konstruktør

    public SnakeView(ViewableSnakeModel viewableSnakeModel) {
        this.setFocusable(true);
        this.setPreferredSize(new Dimension(500, 500));
        this.colorTheme = new DefaultColorTheme();
        this.viewableSnakeModel = viewableSnakeModel;
        this.setBackground(colorTheme.getBackgroundColor());
        
      }
    
      @Override
      protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;
        drawGame(g2);
    }

    /**
   * Metode som tegner spillet.
   * @param g2 Grafikk-objekt
   */
  public void drawGame(Graphics2D g2) {
    Rectangle2D rec = new Rectangle2D.Double(OUTERMARGIN, OUTERMARGIN, getWidth() - 2 * OUTERMARGIN, getHeight() - 2 * OUTERMARGIN);
    g2.setColor(this.colorTheme.getFrameColor());
    g2.fill(rec);
    CellPositionToPixelConverter pixelConverter = new CellPositionToPixelConverter(rec, this.viewableSnakeModel.getDimension(), cellMargin);
    drawCells(g2, viewableSnakeModel.getTilesOnBoard(), pixelConverter, this.colorTheme);
    drawCells(g2, viewableSnakeModel.getSnakeBody(), pixelConverter, this.colorTheme);

    Color scoreColor = this.colorTheme.scoreBoardColor();
    g2.setColor(scoreColor);
    g2.setFont(new Font("Consolas", Font.BOLD, 30));
    g2.drawString(Integer.toString(viewableSnakeModel.getApplesEaten()), getWidth() / 2, 30);

    //Hvis Game Menu kommer det en mørkere og gjennomsiktig farge over brettet med "PLAY SNAKE"
    
    if (viewableSnakeModel.getGameState() == GameState.GAME_MENU){
      Rectangle2D menu = new Rectangle2D.Double(0, 0, getWidth(), getHeight());
      Color gameMenuColor = this.colorTheme.getGameOverColor();
      Color gameMenuText = this.colorTheme.gameOverTextColor();
      g2.setColor(gameMenuColor);
      g2.fill(menu);

      g2.setColor(gameMenuText);
      g2.setFont(new Font("Consolas", Font.BOLD, 40));
      g2.drawString("PLAY SNAKE", getWidth() / 2 - 115, getHeight() / 2 - 20);
      g2.setFont(new Font("Consolas", Font.BOLD, 25));
      g2.drawString("Press SPACE to start the game!", 55, getHeight() / 2 + 40);
    }

    //Hvis Game Over kommer det en mørkere og gjennomsiktig farge over brettet med "GAME OVER"

    else if (viewableSnakeModel.getGameState() == GameState.GAME_OVER){
      Rectangle2D over = new Rectangle2D.Double(0, 0, getWidth(), getHeight());
      Color gameOverColor = this.colorTheme.getGameOverColor();
      Color gameOverText = this.colorTheme.gameOverTextColor();
      g2.setColor(gameOverColor);
      g2.fill(over);

      g2.setColor(gameOverText);
      g2.setFont(new Font("Consolas", Font.BOLD, 40));
      g2.drawString("GAME OVER", getWidth() / 2 - 115, getHeight() / 2 - 80);
      g2.setFont(new Font("Consolas", Font.BOLD, 25));
      g2.drawString("Score: " + Integer.toString(viewableSnakeModel.getApplesEaten()), getWidth() / 2 - 45, getHeight() / 2 -20);
      g2.setFont(new Font("Consolas", Font.BOLD, 15));
      g2.drawString("Press 'R' to get back to menu", getWidth() / 2 - 105, getHeight() / 2 + 40);
    }
    }

    /**
   * Metode som tegner hver celle på brettet.
   * @param g2 Grafikk-objekt
   * @param grid Snakebrettet
   * @param pixel Pixel
   * @param colorTheme For farge på celle
   */
  public static void drawCells(Graphics2D g2, Iterable<GridCell<Character>> grid, CellPositionToPixelConverter pixel, ColorTheme colorTheme) {
    for(GridCell<Character> cell : grid) {
      Rectangle2D rectangle = pixel.getBoundsForCell(cell.pos());
      Color color = colorTheme.getCellColor(cell.value());
      g2.setColor(color);
      g2.fill(rectangle);  
    }
    
}
}
