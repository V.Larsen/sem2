package no.uib.inf101.sem2.grid;

public interface GridDimension {

  /**
   * 
   * @return Antall rader i grid
   */
  int rows();

  /**
   * 
   * @return Antall kolonner i grid
   */
  int cols();
}
