package no.uib.inf101.sem2.grid;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Grid<E> implements IGrid<E> {
    private int rows;
    private int cols;
    private List<List<E>> grid;

    //Konstruktør 1
    public Grid(int rows, int cols){
        this(rows, cols, null);
    }
    //Konstruktør 2
    public Grid(int rows, int cols, E defaltValue){
        this.rows = rows;
        this.cols = cols;
        this.grid = new ArrayList<>();

        for(int r = 0 ; r < rows; r++) {
            this.grid.add(r, new ArrayList<>());
            for(int c = 0 ; c < cols ; c++) {
                grid.get(r).add(c, defaltValue);
            }
        }
    }

    //IMPLEMENTERTE OVERRIDE-METODER

    @Override
    public void set(CellPosition pos, E value) {
        this.grid.get(pos.row()).set(pos.col(), value);
    }

    @Override
    public E get(CellPosition pos) {
        E e = this.grid.get(pos.row()).get(pos.col());
        return e;
    }

    @Override
    public boolean positionIsOnGrid(CellPosition pos) {
        if(pos.row() < 0) return false;
        if(pos.col() < 0) return false;
        if(pos.row() >= rows) return false;
        if(pos.col() >= cols) return false;

        return true;
    }
    @Override
    public int rows() {
        return this.grid.size();
    }
    @Override
    public int cols() {
        return this.grid.get(0).size();
    }
    @Override
    public Iterator<GridCell<E>> iterator() {
        List<GridCell<E>> cells = new ArrayList<>();
        for(int i = 0 ; i < this.rows ; i++) {
            for(int j = 0 ; j < this.cols ; j++) {
                cells.add(new GridCell<E>(new CellPosition(i, j), this.grid.get(i).get(j)));
            }
        }
        return cells.iterator();
    }
    
}
