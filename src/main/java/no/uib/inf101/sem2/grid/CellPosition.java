package no.uib.inf101.sem2.grid;

/**
 * En CellPosition består av en rad og en kolonne
 *
 * @param row  Raden til cellen
 * @param col  Kolonnen til cellen
 */
public record CellPosition(int row, int col) {
    
}
