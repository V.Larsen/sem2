package no.uib.inf101.sem2.grid;

public interface IGrid<E> extends GridDimension, Iterable<GridCell<E>>{
  
  /**
  * Setter en ny verdi på en gitt posisjon på brettet.
  * 
  * @param pos Posisjonen man har lyst til å endre value
  * @param value Value som blir satt
  * @throws IndexOutOfBoundsException Hvis posisjon ikke ekstisterer i grid
  */
  void set(CellPosition pos, E value);
  
  /**
  * Henter nåværende verdi på gitt posisjon
  * 
  * @param pos Posisjon man henter fra
  * @return Den nåværende verdien på gitt posisjon
  * @throws IndexOutOfBoundsException Hvis posisjon ikke ekstisterer i grid
  */
  E get(CellPosition pos);
  
  /**
  * Vurderer om gitt posisjon er innenfor grid
  * 
  * @param pos Posisjonen man sjekker
  * @return True hvis innenfor, false ellers
  */
  boolean positionIsOnGrid(CellPosition pos);
}
