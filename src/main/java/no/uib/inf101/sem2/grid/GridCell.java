package no.uib.inf101.sem2.grid;

/**
 * En CellColor har en CellPosition og en Color
 *
 * @param pos  posisjonen til cellen
 * @param value  fargen til cellen
 */
public record GridCell<E>(CellPosition pos, E value) {
    
}
