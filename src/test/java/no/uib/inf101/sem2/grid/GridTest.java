package no.uib.inf101.sem2.grid;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

/**
* Tester klassen Grid
*/
public class GridTest {
  
  @Test
  void gridTestGetRowsAndCols() {
    IGrid<Integer> grid = new Grid<>(3, 2);
    //Sjekker brettet vi har laget har like mange rader og kolonner vi forventer
    assertEquals(3, grid.rows());
    assertEquals(2, grid.cols());
  }
  
  @Test
  void gridSanityTest() {
    String defaultValue = "x";
    IGrid<String> grid = new Grid<>(3, 2, defaultValue);
    //Sjekker at vi har like mange rader og kolonner vi forventer
    assertEquals(3, grid.rows());
    assertEquals(2, grid.cols());
    //Henter ut en posisjon og sørger for at den har riktig verdi
    assertEquals("x", grid.get(new CellPosition(0, 0)));
    assertEquals("x", grid.get(new CellPosition(2, 1)));
    
    grid.set(new CellPosition(1, 1), "y");
    //Sjekker det samme som ovenfor, bare med ny verdi
    assertEquals("y", grid.get(new CellPosition(1, 1)));
    assertEquals("x", grid.get(new CellPosition(0, 1)));
    assertEquals("x", grid.get(new CellPosition(1, 0)));
    assertEquals("x", grid.get(new CellPosition(2, 1)));
  }
  
  @Test
  void gridCanHoldNull() {
    String defaultValue = "x";
    IGrid<String> grid = new Grid<>(3, 2, defaultValue);
    //Henter ut en posisjon og sørger for at den har riktig verdi
    assertEquals("x", grid.get(new CellPosition(0, 0)));
    assertEquals("x", grid.get(new CellPosition(2, 1)));
    
    grid.set(new CellPosition(1, 1), null);
    //Henter ut en posisjon og sørger for at den har riktig verdi
    assertEquals(null, grid.get(new CellPosition(1, 1)));
    assertEquals("x", grid.get(new CellPosition(0, 1)));
    assertEquals("x", grid.get(new CellPosition(1, 0)));
    assertEquals("x", grid.get(new CellPosition(2, 1)));
  }
  
  @Test
  void gridNullsInDefaultConstructor() {
    IGrid<String> grid = new Grid<>(3, 2);
    //Henter ut en posisjon og sørger for at den har riktig verdi
    assertEquals(null, grid.get(new CellPosition(0, 0)));
    assertEquals(null, grid.get(new CellPosition(2, 1)));
    
    grid.set(new CellPosition(1, 1), "y");
    //Henter ut en posisjon og sørger for at den har riktig verdi
    assertEquals("y", grid.get(new CellPosition(1, 1)));
    assertEquals(null, grid.get(new CellPosition(0, 1)));
    assertEquals(null, grid.get(new CellPosition(1, 0)));
    assertEquals(null, grid.get(new CellPosition(2, 1)));
  }
  
  @Test
  void coordinateIsOnGridTest() {
    IGrid<Double> grid = new Grid<>(3, 2, 0.9);
    //Sjekker om posisjon er innenfor den nye griden
    assertTrue(grid.positionIsOnGrid(new CellPosition(2, 1)));
    assertFalse(grid.positionIsOnGrid(new CellPosition(3, 1)));
    assertFalse(grid.positionIsOnGrid(new CellPosition(2, 2)));
    
    assertTrue(grid.positionIsOnGrid(new CellPosition(0, 0)));
    assertFalse(grid.positionIsOnGrid(new CellPosition(-1, 0)));
    assertFalse(grid.positionIsOnGrid(new CellPosition(0, -1)));
  }
  
  @Test
  void throwsExceptionWhenCoordinateOffGrid() {
    IGrid<String> grid = new Grid<>(3, 2, "x");
    //Kaster exception hvis posisjon er utenfor grid
    try {
      @SuppressWarnings("unused")
      String x = grid.get(new CellPosition(3, 1));
      fail();
    } catch (IndexOutOfBoundsException e) {
      // Test passed
    }
  }
  
  @Test
  void testIterator() {
    IGrid<String> grid = new Grid<>(3, 2, "x");
    grid.set(new CellPosition(0, 0), "a");
    grid.set(new CellPosition(1, 1), "b");
    grid.set(new CellPosition(2, 1), "c");
    
    List<GridCell<String>> items = new ArrayList<>();
    for (GridCell<String> coordinateItem : grid) {
      items.add(coordinateItem);
    }
    //Sjekker listen for GridCell-objekter som bør være der
    assertEquals(3 * 2, items.size());
    assertTrue(items.contains(new GridCell<String>(new CellPosition(0, 0), "a")));
    assertTrue(items.contains(new GridCell<String>(new CellPosition(1, 1), "b")));
    assertTrue(items.contains(new GridCell<String>(new CellPosition(2, 1), "c")));
    assertTrue(items.contains(new GridCell<String>(new CellPosition(0, 1), "x")));
  }
}
