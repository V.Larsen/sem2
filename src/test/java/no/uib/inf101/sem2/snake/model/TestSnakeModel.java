package no.uib.inf101.sem2.snake.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import no.uib.inf101.sem2.grid.CellPosition;
import no.uib.inf101.sem2.grid.GridCell;
import no.uib.inf101.sem2.snake.model.snakegame.Snake;

public class TestSnakeModel {

    @Test
    public void initialPositionofSnake() {
    //Får slangen på brettet
    Snake snakeBody = Snake.snakeOnBoard('S');

    List<GridCell<Character>> objs = new ArrayList<>();
    for (GridCell<Character> gc : snakeBody) {
        objs.add(gc);
    }

    //Sjekker at vi får de forventede GridCell-objektene
    assertEquals(3, objs.size());
    assertTrue(objs.contains(new GridCell<>(new CellPosition(7, 7), 'S')));
    assertTrue(objs.contains(new GridCell<>(new CellPosition(7, 8), 'S')));
    assertTrue(objs.contains(new GridCell<>(new CellPosition(7,9), 'S')));
    }
    
    @Test
    public void testmoveSnake() {
    SnakeBoard board = new SnakeBoard(15, 15);
    SnakeModel model = new SnakeModel(board);

    List<GridCell<Character>> snakeCells = new ArrayList<>();
    for (GridCell<Character> gc : model.getSnakeBody()) {
        snakeCells.add(gc);
    }
    //Sjekker at det er et mulig flytt
    assertTrue(model.moveSnake(4, 0));
    assertTrue(model.moveSnake(0, 4));
    }

    @Test
    public void testmoveSnakeIteration() {
    SnakeBoard board = new SnakeBoard(15, 15);
    SnakeModel model = new SnakeModel(board);
    model.moveSnake(1, 0);

    List<GridCell<Character>> snakeCells = new ArrayList<>();
    for (GridCell<Character> gc : model.getSnakeBody()) {
        snakeCells.add(gc);
    }
    //Sjekker at det er et mulig flytt og slangens posisjon
    assertEquals(3, snakeCells.size());
    assertTrue(snakeCells.contains(new GridCell<>(new CellPosition(8, 7), 'S')));
    assertTrue(snakeCells.contains(new GridCell<>(new CellPosition(7, 7), 'S')));
    assertTrue(snakeCells.contains(new GridCell<>(new CellPosition(7,8), 'S')));
    }

    @Test
    public void testmoveSnakeOutOfBoard() {
    SnakeBoard board = new SnakeBoard(15, 15);
    SnakeModel model = new SnakeModel(board);

    List<GridCell<Character>> snakeCells = new ArrayList<>();
    for (GridCell<Character> gc : model.getSnakeBody()) {
        snakeCells.add(gc);
    }
    //Sjekker at det er et mulig flytt og slangens posisjon
    assertFalse(model.moveSnake(0, 10));
    //Sørger for at posisjonen til slangen ikke har endret seg.
    assertTrue(snakeCells.contains(new GridCell<>(new CellPosition(7, 7), 'S')));
    assertTrue(snakeCells.contains(new GridCell<>(new CellPosition(7, 8), 'S')));
    assertTrue(snakeCells.contains(new GridCell<>(new CellPosition(7,9), 'S')));
    }

    @Test
    public void testmoveSnakeAndGrow() {
    SnakeBoard board = new SnakeBoard(15, 15);
    SnakeModel model = new SnakeModel(board);
    board.set(new CellPosition(7, 6), 'A');
    model.moveSnake(0, -1);

    List<GridCell<Character>> snakeCells = new ArrayList<>();
    for (GridCell<Character> gc : model.getSnakeBody()) {
        snakeCells.add(gc);
    }

    //Sjekker at slangen gror fra 3 til 4 og dens posisjon.
    assertEquals(4, snakeCells.size());
    assertTrue(snakeCells.contains(new GridCell<>(new CellPosition(7, 6), 'S')));
    assertTrue(snakeCells.contains(new GridCell<>(new CellPosition(7, 7), 'S')));
    assertTrue(snakeCells.contains(new GridCell<>(new CellPosition(7,8), 'S')));
    }

    @Test
    public void testNoReverseMove() {
    SnakeBoard board = new SnakeBoard(15, 15);
    SnakeModel model = new SnakeModel(board);
    //Flytter slangen først et hakk til venstre, før jeg prøver å flytte den tilbake i seg selv
    model.moveSnake(0, -1);
    model.moveSnake(0, 1);

    List<GridCell<Character>> snakeCells = new ArrayList<>();
    for (GridCell<Character> gc : model.getSnakeBody()) {
        snakeCells.add(gc);
    }

    //Sjekker at slangen ikke flytter tilbake, men kun fram
    assertEquals(3, snakeCells.size());
    assertTrue(snakeCells.contains(new GridCell<>(new CellPosition(7, 6), 'S')));
    assertTrue(snakeCells.contains(new GridCell<>(new CellPosition(7, 7), 'S')));
    assertTrue(snakeCells.contains(new GridCell<>(new CellPosition(7,8), 'S')));
    }
}
