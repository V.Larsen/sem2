package no.uib.inf101.sem2.snake.model.snakemovement;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import no.uib.inf101.sem2.grid.CellPosition;
import no.uib.inf101.sem2.grid.GridCell;
import no.uib.inf101.sem2.snake.model.snakegame.Snake;

public class TestSnake {

    @Test
    public void testHashCodeAndEquals() {
        Snake t1 = Snake.snakeOnBoard('S');
        Snake t2 = Snake.snakeOnBoard('S');
        Snake t3 = Snake.snakeOnBoard('S');
        t3.move(new CellPosition(8, 7));
        Snake s1 = Snake.snakeOnBoard('S');
        Snake s2 = Snake.snakeOnBoard('S');
        

        assertEquals(t1, t2);
        assertEquals(s1, s2);
        assertEquals(t1.hashCode(), t2.hashCode());
        assertNotEquals(t1, t3);
    }

    @Test
    public void moveSnakeUp() {
    //Får slangen på brettet og sjekker om hodet beveger seg opp og at kroppen følger etter
    Snake snakeBody = Snake.snakeOnBoard('S');
    CellPosition newPos = new CellPosition(snakeBody.getPos().row() -1, snakeBody.getPos().col() + 0);
    snakeBody.move(newPos);

    List<GridCell<Character>> objs = new ArrayList<>();
    for (GridCell<Character> gc : snakeBody) {
        objs.add(gc);
    }

    //Sjekker at vi får de forventede GridCell-objektene
    assertEquals(3, objs.size());
    assertTrue(objs.contains(new GridCell<>(new CellPosition(6, 7), 'S')));
    assertTrue(objs.contains(new GridCell<>(new CellPosition(7, 7), 'S')));
    assertTrue(objs.contains(new GridCell<>(new CellPosition(7, 8), 'S')));
    }
    
    @Test
    public void moveSnakeDown() {
    //Får slangen på brettet og sjekker om hodet beveger seg ned og at kroppen følger etter
    Snake snakeBody = Snake.snakeOnBoard('S');
    CellPosition newPos = new CellPosition(snakeBody.getPos().row() + 1, snakeBody.getPos().col() + 0);
    snakeBody.move(newPos);

    List<GridCell<Character>> objs = new ArrayList<>();
    for (GridCell<Character> gc : snakeBody) {
        objs.add(gc);
    }

    //Sjekker at vi får de forventede GridCell-objektene
    assertEquals(3, objs.size());
    assertTrue(objs.contains(new GridCell<>(new CellPosition(8, 7), 'S')));
    assertTrue(objs.contains(new GridCell<>(new CellPosition(7, 7), 'S')));
    assertTrue(objs.contains(new GridCell<>(new CellPosition(7, 8), 'S')));
    }

    @Test
    public void moveSnakeLeft() {
    //Får slangen på brettet og sjekker om hodet beveger seg til venstre og at kroppen følger etter
    Snake snakeBody = Snake.snakeOnBoard('S');
    CellPosition newPos = new CellPosition(snakeBody.getPos().row() + 0, snakeBody.getPos().col() - 1);
    snakeBody.move(newPos);

    List<GridCell<Character>> objs = new ArrayList<>();
    for (GridCell<Character> gc : snakeBody) {
        objs.add(gc);
    }

    //Sjekker at vi får de forventede GridCell-objektene
    assertEquals(3, objs.size());
    assertTrue(objs.contains(new GridCell<>(new CellPosition(7, 6), 'S')));
    assertTrue(objs.contains(new GridCell<>(new CellPosition(7, 7), 'S')));
    assertTrue(objs.contains(new GridCell<>(new CellPosition(7, 8), 'S')));
    }

    @Test
    public void growSnake() {
    //Får slangen på brettet og sjekker om slangen gror
    Snake snakeBody = Snake.snakeOnBoard('S');
    CellPosition newPos = new CellPosition(7, 6);
    snakeBody.grow(newPos);

    List<GridCell<Character>> objs = new ArrayList<>();
    for (GridCell<Character> gc : snakeBody) {
        objs.add(gc);
    }

    //Sjekker at vi får de forventede GridCell-objektene
    assertEquals(4, objs.size());
    assertTrue(objs.contains(new GridCell<>(new CellPosition(7, 6), 'S')));
    assertTrue(objs.contains(new GridCell<>(new CellPosition(7, 7), 'S')));
    assertTrue(objs.contains(new GridCell<>(new CellPosition(7, 8), 'S')));
    assertTrue(objs.contains(new GridCell<>(new CellPosition(7, 9), 'S')));
    }

    @Test
    public void isSnakeHereTest() {
    //Får slangen på brettet og sjekker om slangen er på neste posisjon.
    Snake snakeBody = Snake.snakeOnBoard('S');
    CellPosition newPos = new CellPosition(7, 7);
    CellPosition newPos1 = new CellPosition(7, 8);
    CellPosition newPos2 = new CellPosition(7, 9);
    CellPosition fakePos = new CellPosition(7, 6);
    CellPosition fakePos1 = new CellPosition(7, 10);
    List<GridCell<Character>> objs = new ArrayList<>();
    for (GridCell<Character> gc : snakeBody) {
        objs.add(gc);
    }

    //Sjekker at vi får de forventede GridCell-objektene
    assertTrue(objs.contains(new GridCell<>(new CellPosition(newPos.row(), newPos.col()), 'S')));
    assertTrue(objs.contains(new GridCell<>(new CellPosition(newPos1.row(), newPos1.col()), 'S')));
    assertTrue(objs.contains(new GridCell<>(new CellPosition(newPos2.row(), newPos2.col()), 'S')));
    assertFalse(objs.contains(new GridCell<>(new CellPosition(fakePos.row(), fakePos.col()), 'S')));
    assertFalse(objs.contains(new GridCell<>(new CellPosition(fakePos1.row(), fakePos1.col()), 'S')));
    }
}
