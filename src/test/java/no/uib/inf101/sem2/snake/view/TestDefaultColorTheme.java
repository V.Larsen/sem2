package no.uib.inf101.sem2.snake.view;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.awt.Color;

import org.junit.jupiter.api.Test;

public class TestDefaultColorTheme {
    @Test
    public void sanityTestDefaultColorTheme() {
        ColorTheme colors = new DefaultColorTheme();
        //Sjekker om fargene stemmer overens
        assertEquals(null, colors.getBackgroundColor());
        assertEquals(new Color(0, 0, 0, 0), colors.getFrameColor());
        assertEquals(Color.decode("#6EF07E"), colors.getCellColor('-'));
        assertEquals(Color.RED, colors.getCellColor('A'));
        assertEquals(Color.decode("#038914"), colors.getCellColor('S'));
        assertThrows(IllegalArgumentException.class, () -> colors.getCellColor('\n'));
}

}
