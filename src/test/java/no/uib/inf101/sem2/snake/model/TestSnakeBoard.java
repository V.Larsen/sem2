package no.uib.inf101.sem2.snake.model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import no.uib.inf101.sem2.grid.CellPosition;

public class TestSnakeBoard {
    
    @Test
    public void testPrettyString() {
        SnakeBoard board = new SnakeBoard(3, 4);
        board.set(new CellPosition(0, 0), 'S');
        board.set(new CellPosition(0, 3), 'A');
        board.set(new CellPosition(1, 0), 'L');
        board.set(new CellPosition(1, 3), 'A');
        board.set(new CellPosition(2, 0), 'S');
        board.set(new CellPosition(2, 3), 'A');
        String expected = String.join("\n", new String[] {
            "S--A",
            "L--A",
            "S--A"
        });
        assertEquals(expected, board.prettyString());
    }
}
