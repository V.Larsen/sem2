package no.uib.inf101.sem2.snake.model.snakemovement;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import no.uib.inf101.sem2.grid.CellPosition;
import no.uib.inf101.sem2.grid.GridCell;
import no.uib.inf101.sem2.snake.model.SnakeBoard;
import no.uib.inf101.sem2.snake.model.SnakeModel;

public class TestFood {
    @Test
    public void testFoodSpawn() {
    SnakeBoard board = new SnakeBoard(15, 15);
    SnakeModel model = new SnakeModel(board);
    
    List<GridCell<Character>> snakeCells = new ArrayList<>();
    for (GridCell<Character> gc : model.getTilesOnBoard()) {
        if (gc.value() == 'A') {
            snakeCells.add(gc);
        }
    }
    //Sjekker at det kommer et eple på brettet.
    assertEquals(1, snakeCells.size());
    }

    @Test
    public void testFoodSpawnAfterEaten() {
    SnakeBoard board = new SnakeBoard(15, 15);
    SnakeModel model = new SnakeModel(board);
    //Setter et nytt eple på brettet selv om det allerede er ett.
    //Gjør dette fordi vi ikke vet hvor eple befinner seg.
    //Det vil derfor være 2 epler på brettet.
    board.set(new CellPosition(7, 6), 'A');
    model.moveSnake(0, -1);

    List<GridCell<Character>> snakeCells = new ArrayList<>();
    for (GridCell<Character> gc : model.getTilesOnBoard()) {
        if (gc.value() == 'A') {
            snakeCells.add(gc);
        }
    }
    //Sjekker at det kommer et nytt eple på brettet etter det gamle er spist.
    assertEquals(2, snakeCells.size());
    }
}
